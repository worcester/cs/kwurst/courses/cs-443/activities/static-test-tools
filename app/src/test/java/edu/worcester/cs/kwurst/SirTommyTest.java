package edu.worcester.cs.kwurst;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.worcester.cs.kwurst.SirTommy;

class SirTommyTest {
    SirTommy game;
    private PrintStream oldOut; 
    private ByteArrayOutputStream outContent;

	@BeforeEach
	void setUp() throws IOException {
        game = new SirTommy();
        
        oldOut = System.out; 
	    outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent)); 
	}
	
	@AfterEach
	void tearDown() {
	    System.setOut(oldOut); 
	}

	@Test
	void testCheckForGameOverWhenPlayerQuits() {
	    oldOut = System.out; 
	    outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent)); 
	    
		assertTrue(game.checkForGameOver(true));
	    assertEquals(outContent.toString(), " You lost - try again.\n"); 
	}

	@Test
	void testGetSrcPileWithNewGame() {
		assertNotNull(game.getSrcPile("D"), "New game has cards in Deck pile");
		assertNull(game.getSrcPile("W0"), "New game has no cards in Waste piles");
		assertNull(game.getSrcPile("W1"), "New game has no cards in Waste piles");
		assertNull(game.getSrcPile("W2"), "New game has no cards in Waste piles");
		assertNull(game.getSrcPile("W3"), "New game has no cards in Waste piles");
		assertNull(game.getSrcPile("X"), "Source piles are only D, W0, W1, W2, W3");
	}

}
